#!/bin/bash

IF=wlan0:1
IP=10.11.12.13
PORT=1415
DISTDIR=/tmp/distdir
SRVDIR=/tmp/srvdir

# serve files from current directory, will break if there is no "main.py", as
# it's probably wrong working dir

if [ ! -f main.py ];then
    echo "no main.py in working directory, probably wrong file"
    exit 1
fi

mkdir -p $DISTDIR

set -ex

rsync --delete --exclude ".*" --exclude "~*" --exclude "bin" -avz . $DISTDIR

cd $DISTDIR

MDSUM=$(tar -cf - . | md5sum | cut -d" " -f1)
NODETYPE=$(python -c "from config import OTA;print(OTA['node_type'])")

mkdir -p $SRVDIR/$NODETYPE


echo $MDSUM > $SRVDIR/$NODETYPE/version.txt

tar -vcf $SRVDIR/$NODETYPE/$MDSUM.tar .

cd $SRVDIR

if mkdir .srvlock ; then
    echo "acquired lock, running server"
    
    sudo ifconfig $IF $IP

    python -m http.server --bind $IP $PORT

    rm -rv .srvlock

    echo "removed lock"
else
    echo "server already running, skipping"
fi






