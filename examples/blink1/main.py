from machine import Pin
from time import sleep

import otacli
if otacli.check():
    print("this line will be never reached")


led = Pin(2, Pin.OUT)

while True:
    led.value(1)
    sleep(0.05)
    led.value(0)
    sleep(0.1)

    
